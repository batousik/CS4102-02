# README #

The 3D Rendering Application.

### What is this project for? ###

* Understand the key principles behind various techniques frequently used for the rendering of 3D objects
* Get hands-on experience with their implementation and manipulation
* Version: 1.0.0

### How do I get set up? ###

* Install Maven and Java
* Execute "mvn install"
* Execute "java -jar target\rendering3d-1.0.0-jar-with-dependencies.jar"
