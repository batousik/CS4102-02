package ac.uk.standrews.cs.cs4201.rendering3d;


import ac.uk.standrews.cs.cs4201.redering3d.engine.Triangle;
import ac.uk.standrews.cs.cs4201.redering3d.engine.filereader.ModelReader;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class FileIOTest {
    @Test
    public void resourceLoadingTest() {
        ArrayList<Triangle> ts = ModelReader.readFaceShape();
        Triangle t = ts.get(106465);
        double[] v1 = t.getM().getColumn(0);
        double[] v2 = t.getM().getColumn(1);
        double[] v3 = t.getM().getColumn(2);

        assertEquals(58820.382813d, v1[0], 0.001);
        assertEquals(-23316.123047d, v1[1], 0.001);
        assertEquals(-19067.812500d, v1[2], 0.001);
        assertEquals(1, v1[3], 0.001);

        assertEquals(59253.386719d, v2[0], 0.001);
        assertEquals(-22994.978516d, v2[1], 0.001);
        assertEquals(-18081.660156d, v2[2], 0.001);
        assertEquals(1, v2[3], 0.001);

        assertEquals(59073.644531d, v3[0], 0.001);
        assertEquals(-22708.927734d, v3[1], 0.001);
        assertEquals(-18775.916016d, v3[2], 0.001);
        assertEquals(1, v3[3], 0.001);
    }

    @Test
    public void fieldVectorTest() {
        double[] vals = new double[]{
                1d,
                1d,
                0d,
                0d
        };
        RealVector v = new ArrayRealVector(vals);

        double val = v.getEntry(3);

        System.out.println(val);
        System.out.println(-18775.916016d);
        assertEquals(0d, val, 0.001);
    }
}
