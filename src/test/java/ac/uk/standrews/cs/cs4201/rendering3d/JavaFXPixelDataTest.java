package ac.uk.standrews.cs.cs4201.rendering3d;

import ac.uk.standrews.cs.cs4201.ui.JavaFXThreadingRule;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritablePixelFormat;
import org.junit.Rule;
import org.junit.Test;

/**
 * Created by Tsar on 11.04.2017.
 */
public class JavaFXPixelDataTest {
    @Rule
    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();

    @Test
    public void pixelDataTest() {
        Image src = new Image("img/test.png");
        PixelReader reader = src.getPixelReader();
        /*
        [black, white, blue, red, green]

        Image Data: (rgba)
            Byte: 0 HEX: 0x00 Int: 0
            Byte: 0 HEX: 0x00 Int: 0
            Byte: 0 HEX: 0x00 Int: 0
            Byte: -1 HEX: 0xFF Int: 255

            Byte: -1 HEX: 0xFF Int: 255
            Byte: -1 HEX: 0xFF Int: 255
            Byte: -1 HEX: 0xFF Int: 255
            Byte: -1 HEX: 0xFF Int: 255

            Byte: -24 HEX: 0xE8 Int: 232
            Byte: -94 HEX: 0xA2 Int: 162
            Byte: 0 HEX: 0x00 Int: 0
            Byte: -1 HEX: 0xFF Int: 255

            Byte: 36 HEX: 0x24 Int: 36
            Byte: 28 HEX: 0x1C Int: 28
            Byte: -19 HEX: 0xED Int: 237
            Byte: -1 HEX: 0xFF Int: 255

            Byte: 38 HEX: 0x26 Int: 38
            Byte: -47 HEX: 0xD1 Int: 209
            Byte: -112 HEX: 0x90 Int: 144
            Byte: -1 HEX: 0xFF Int: 255
         */
        int w = 5;
        int h = 1;
        byte[] arr = new byte[w * h * 4];

        reader.getPixels(0, 0, w, h, WritablePixelFormat.getByteBgraInstance(), arr, 0, 4);
        for (int i = 0; i < arr.length; i++) {
            byte b = arr[i];
            if (i % 4 == 0) System.out.println();
            System.out.println("Byte: " + b + " HEX: " + String.format("0x%02X",
                                                                       b
            ) + " Int: " + String.valueOf(b & 0xff));
        }
    }

    @Test
    public void byteTest() {
        byte b = (byte) 0;
        System.out.println("Byte: " + b + " HEX: " + String.format("0x%02X", b) + " Int: " + String.valueOf(b & 0xff));
    }
}
