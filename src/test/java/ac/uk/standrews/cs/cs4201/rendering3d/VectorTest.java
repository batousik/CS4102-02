package ac.uk.standrews.cs.cs4201.rendering3d;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Tsar on 13.04.2017.
 */
public class VectorTest {
    @Test
    public void vectorSubtractionTest() {
        RealVector v1 = new ArrayRealVector(new double[]{
                5,
                5,
                5
        });
        RealVector v2 = new ArrayRealVector(new double[]{
                15,
                25,
                35
        });

        RealVector vm = v2.subtract(v1);
        RealVector vn = v2.ebeMultiply(v1);

        assertEquals(vm.getEntry(0), 10, 0.1);
    }
}
