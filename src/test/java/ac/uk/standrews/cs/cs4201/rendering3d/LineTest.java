package ac.uk.standrews.cs.cs4201.rendering3d;

import ac.uk.standrews.cs.cs4201.redering3d.controller.Point2DDouble;
import ac.uk.standrews.cs.cs4201.redering3d.engine.LineCreator;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Tsar on 11.04.2017.
 */
public class LineTest {
    @Test
    public void bresenhamsLineTest() {
        double x0 = 0;
        double y0 = 0;
        double x1 = 10;
        double y1 = 15;
        ArrayList<Point2DDouble> points = LineCreator.getBresenhamsLineV2(x0, y0, x1, y1);

        for (Point2DDouble p : points) {
            System.out.println(p);
        }
    }
}
