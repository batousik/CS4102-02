package ac.uk.standrews.cs.cs4201.redering3d.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class TestMaterial extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/material_fx_test.fxml"));
        Scene scene = new Scene(root, 1000, 600);
        String material_css = this.getClass().getClassLoader().getResource("css/material-fx-v0_3.css").toExternalForm();
        String material_switch_css =
                this.getClass().getClassLoader().getResource("css/materialfx-toggleswitch.css").toExternalForm();
        String rendering3d_css = this.getClass().getClassLoader().getResource("css/rendering3d.css").toExternalForm();

        scene.getStylesheets().clear();
        scene.getStylesheets().add(material_css);
        scene.getStylesheets().add(material_switch_css);
        scene.getStylesheets().add(rendering3d_css);

        stage.setTitle("3D Rendering JavaFX Application");
        stage.setScene(scene);
        stage.show();
    }
}