package ac.uk.standrews.cs.cs4201.redering3d.controller;

public class Pixel {
    public int x;
    public int y;

    public byte r;
    public byte g;
    public byte b;
    public byte a;

    /**
     * Creates non transparent blue pixel
     *
     * @param x
     * @param y
     */
    public Pixel(int x, int y) {
        this.x = x;
        this.y = y;

        this.r = -24;
        this.g = -94;
        this.b = 0;
        this.a = -1;
    }

    public Pixel(int x, int y, byte r, byte g, byte b, byte a) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
}
