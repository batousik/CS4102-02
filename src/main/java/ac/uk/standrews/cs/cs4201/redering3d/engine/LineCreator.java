package ac.uk.standrews.cs.cs4201.redering3d.engine;


import ac.uk.standrews.cs.cs4201.redering3d.controller.Point2DDouble;

import java.util.ArrayList;


public class LineCreator {
    /**
     * \
     * No octants!
     *
     * @param x0
     * @param y0
     * @param x1
     * @param y1
     * @return
     */
    public static ArrayList<Point2DDouble> getBresenhamsLine(double x0, double y0, double x1, double y1) {
        ArrayList<Point2DDouble> points = new ArrayList<>();
        double deltax = x1 - x0;
        double deltay = y1 - y0;
        double deltaerr = Math.abs(deltay / deltax);  // Assume deltax != 0 (line is not vertical),
        // note that this division needs to be done in a way that preserves the fractional part
        double error = deltaerr - 0.5d;
        int y = (int) y0;

        for (int x = (int) x0; x <= (int) x1; x++) {
            points.add(new Point2DDouble(x, y));
            error = error + deltaerr;
            if (error >= 0.5) {
                y++;
            }
            error -= 1.0;
        }
        return points;
    }

    /**
     * Universal
     *
     * @param xd0
     * @param yd0
     * @param xd1
     * @param yd1
     * @return
     */
    public static ArrayList<Point2DDouble> getBresenhamsLineV2(double xd0, double yd0, double xd1, double yd1) {
        ArrayList<Point2DDouble> points = new ArrayList<>();
        int x1 = (int) xd0, x2 = (int) xd1, y1 = (int) yd0, y2 = (int) yd1;

        // delta of exact value and rounded value of the dependant variable
        int d = 0;

        int dy = Math.abs(y2 - y1);
        int dx = Math.abs(x2 - x1);

        int dy2 = (dy << 1); // slope scaling factors to avoid floating
        int dx2 = (dx << 1); // point

        int ix = x1 < x2 ? 1 : -1; // increment direction
        int iy = y1 < y2 ? 1 : -1;

        if (dy <= dx) {
            for (; ; ) {
                points.add(new Point2DDouble(x1, y1));
                if (x1 == x2) break;
                x1 += ix;
                d += dy2;
                if (d > dx) {
                    y1 += iy;
                    d -= dx2;
                }
            }
        } else {
            for (; ; ) {
                points.add(new Point2DDouble(x1, y1));
                if (y1 == y2) break;
                y1 += iy;
                d += dx2;
                if (d > dy) {
                    x1 += ix;
                    d -= dy2;
                }
            }
        }

        return points;
    }

}
