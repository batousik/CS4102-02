package ac.uk.standrews.cs.cs4201.redering3d.controller;

/**
 * Created by Tsar on 11.04.2017.
 */
public class Point2DDouble {
    public double x, y;

    public Point2DDouble() {
    }

    public Point2DDouble(double x, double y) {
        this.x = x;
        this.y = y;
    }


    @Override
    public String toString() {
        return "x: " + x + " y: " + y;
    }
}
