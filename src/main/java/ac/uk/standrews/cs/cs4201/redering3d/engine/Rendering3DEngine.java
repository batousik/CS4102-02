package ac.uk.standrews.cs.cs4201.redering3d.engine;


import ac.uk.standrews.cs.cs4201.redering3d.controller.Point2DDouble;
import ac.uk.standrews.cs.cs4201.redering3d.engine.filereader.ModelReader;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

import static ac.uk.standrews.cs.cs4201.redering3d.engine.math.MC.*;

public class Rendering3DEngine extends Thread {
    private static final Logger LOGGER = Logger.getLogger(Rendering3DEngine.class.getName());
    private final int TRIANGLE_BUFFER_QUEUE_SIZE = 1000;
    private ArrayList<Triangle> triangles;
    private boolean running;
    private boolean invalidated;
    private boolean wasInvalidated;
    private Point2DDouble centerScreen;
    private Point2DDouble dimension;
    private Point2DDouble deltaXY;
    private double scale;
    private String svgData;
    private Queue<Triangle[]> triangleBufferQueue;
    private int rotationX;
    private int rotationY;
    private int rotationZ;
    private int zoom;
    private String projection;


    public Rendering3DEngine() {
        setRunning(true);
        setInvalidated(true);
        setDefaults();
        triangles = ModelReader.readFaceShape();
        ArrayList<RealVector> grayscale = ModelReader.readFaceTexture();
        for (int i = 0; i < grayscale.size(); i++) {
            triangles.get(i).addSVector(grayscale.get(i));
        }
        triangleBufferQueue = new LinkedList<>();
    }

    @Override
    public void run() {
        LOGGER.info("3DRendering engine started");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (running) {
            try {
                if (isInvalidated() && dimension.x > 0) {
                    render();
                    setInvalidated(false);
                    setWasInvalidated(true);
                } else {
                    Thread.sleep(50);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LOGGER.info("3DRendering engine terminated");
    }

    public synchronized boolean isInvalidated() {
        return invalidated;
    }

    public synchronized void setInvalidated(boolean invalidated) {
        this.invalidated = invalidated;
    }

    public synchronized String getSvgData() {
        return svgData;
    }

    private synchronized void setSvgData(String svgData) {
        this.svgData = svgData;
    }

    public synchronized boolean isRunning() {
        return running;
    }

    public synchronized void setRunning(boolean running) {
        this.running = running;
    }

    public synchronized boolean isWasInvalidated() {
        return wasInvalidated;
    }

    public synchronized void setWasInvalidated(boolean wasInvalidated) {
        this.wasInvalidated = wasInvalidated;
    }

    public synchronized Queue<Triangle[]> getTriangleBufferQueue() {
        return triangleBufferQueue;
    }

    public synchronized void setTriangleBufferQueue(Queue<Triangle[]> triangleBufferQueue) {
        this.triangleBufferQueue = triangleBufferQueue;
    }

    private void render() {
        LOGGER.info("Started Rendering");
        long time = System.nanoTime();
        singleTransformation();
        long f = (System.nanoTime() - time) / 1000 / 1000;
        LOGGER.info("Finished Rendering, took " + f + "ms");
    }

    private void singleTransformation() {
        for (Triangle t : triangles) {
            t.performFullTransformation(constructFullTransformation(), new ArrayRealVector(new double[]{
                    0,
                    0,
                    1
            }));
        }


        Collections.sort(triangles);

        Queue<Triangle[]> triangleTempQueue = new LinkedList<>();

        Triangle[] trianglesChunk = new Triangle[TRIANGLE_BUFFER_QUEUE_SIZE];
        for (int i = 0; i < triangles.size(); i++) {
            if (i % TRIANGLE_BUFFER_QUEUE_SIZE == 0) {

                triangleTempQueue.add(trianglesChunk);
                trianglesChunk = new Triangle[TRIANGLE_BUFFER_QUEUE_SIZE];
            }
            Triangle t = triangles.get(i);
            trianglesChunk[i % TRIANGLE_BUFFER_QUEUE_SIZE] = t;
        }
        triangleTempQueue.add(trianglesChunk);
        setTriangleBufferQueue(triangleTempQueue);
    }

    @Deprecated
    private String stepByStep() {
        StringBuilder sb = new StringBuilder();
        RealMatrix dXdY = construct_dXdY();
        for (int i = 0; i < triangles.size(); i++) {
//            if (i % 10000 == 0) LOGGER.info("Render Progress: " + i / 106466d + "%");
            Triangle t = triangles.get(i);
            t.reset();
            t.scale(scale);
            t.invertY();
            t.updateCenter(dXdY);
            sb.append(t.getSVGPath());
        }
        return sb.toString();
    }

    public void setDefaults() {
        this.deltaXY = new Point2DDouble(0d, 0d);
        setInvalidated(true);
    }

    public void setScale(double scale) {
        this.scale = scale;
    }


    public void updateDeltaXY(Point2DDouble deltaXY) {
        this.deltaXY.x += deltaXY.x;
        this.deltaXY.y += deltaXY.y;
        setInvalidated(true);
    }

    @Deprecated
    private RealMatrix construct_dXdY() {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        centerScreen.x + deltaXY.x,
                        centerScreen.x + deltaXY.x,
                        centerScreen.x + deltaXY.x
                },
                {
                        centerScreen.y + deltaXY.y,
                        centerScreen.y + deltaXY.y,
                        centerScreen.y + deltaXY.y
                },
                {
                        0d,
                        0d,
                        0d
                }
        });
    }

    public void updateDimension(Point2DDouble dimensions) {
        this.centerScreen = new Point2DDouble(dimensions.x / 2d, dimensions.y / 2d);
        this.dimension = dimensions;
        setInvalidated(true);
    }

    private RealMatrix constructFullTransformation() {
        RealMatrix temp = translation_matrix(deltaXY.x + centerScreen.x, deltaXY.y + centerScreen.y, zoom).multiply(
                scale_matrix(scale, scale, scale)).multiply(x_rot_matrix(radians(rotationX))).multiply(y_rot_matrix(
                radians(rotationY))).multiply(z_rot_matrix(radians(rotationZ))).multiply(y_reflect_matrix());
        return temp;
    }

    public void setRotationX(int rotationX) {
        this.rotationX = rotationX;
        setInvalidated(true);
    }

    public void setRotationY(int rotationY) {
        this.rotationY = rotationY;
        setInvalidated(true);
    }

    public void setRotationZ(int rotationZ) {
        this.rotationZ = rotationZ;
        setInvalidated(true);
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
        setInvalidated(true);
    }

    public void setProjection(String projection) {
        this.projection = projection;
        setInvalidated(true);
    }

    public static final class PROJECTION {
        public static final String ORTHOGRAPHIC = "Orthographic", PERSPECTIVE = "Perspective";
    }
}
