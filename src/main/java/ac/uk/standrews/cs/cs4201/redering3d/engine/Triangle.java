package ac.uk.standrews.cs.cs4201.redering3d.engine;


import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Triangle implements Comparable<Triangle> {
    private RealMatrix m;
    private RealMatrix relM;
    private double s;

    //    private RealVector normal;
    private RealVector centroid;
    private ArrayRealVector unitNormal;
    private double relS;
    private RealVector sVector;
    private RealVector relSVector;

    public Triangle(RealMatrix m) {
        this.m = m;
        relM = m;
        calculateCentroid();
        createS();
    }


    public void addS(double s) {
        this.s = s;
        relS = s;
    }


    private void createS() {
        s = 0;
    }


    private void calculateCentroid() {
        double[] xVals = relM.getRow(0);
        double[] yVals = relM.getRow(1);
        double[] zVals = relM.getRow(2);

        double[] centroidData = new double[]{
                xVals[0] + xVals[1] + xVals[2] / 3d,
                yVals[0] + yVals[1] + yVals[2] / 3d,
                zVals[0] + zVals[1] + zVals[2] / 3d
        };
        centroid = new ArrayRealVector(centroidData);
    }

    private void calculateNormal() {
        RealVector v1 = relM.getColumnVector(0);
        RealVector v2 = relM.getColumnVector(1);
        RealVector v3 = relM.getColumnVector(2);

        double[] u = v2.subtract(v1).toArray();
        double[] v = v3.subtract(v2).toArray();

        double[] normal = new double[]{
                u[1] * v[2] - u[2] * v[1],
                u[2] * v[0] - u[0] * v[2],
                u[0] * v[1] - u[1] * v[0]
        };

        double normalLength = Math.sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
        normal[0] /= normalLength;
        normal[1] /= normalLength;
        normal[2] /= normalLength;
        unitNormal = new ArrayRealVector(normal);
    }

    public RealMatrix getM() {
        return m;
    }

    public void reset() {
        relM = m;
    }

    public void scale(double scalar_scaler) {
        relM = relM.scalarMultiply(scalar_scaler);
    }

    public void updateCenter(RealMatrix dXdY) {
        relM = relM.add(dXdY);
    }

    public void performFullTransformation(RealMatrix transformer, RealVector pointLight) {
        RealMatrix m1 = m.getColumnMatrix(0);
        RealMatrix m2 = m.getColumnMatrix(1);
        RealMatrix m3 = m.getColumnMatrix(2);

        relM = new Array2DRowRealMatrix(new double[4][3]);

        relM.setColumnMatrix(0, transformer.multiply(m1));
        relM.setColumnMatrix(1, transformer.multiply(m2));
        relM.setColumnMatrix(2, transformer.multiply(m3));
        calculateCentroid();
        calculateNormal();
        RealVector x = pointLight.ebeMultiply(pointLight);
        double coeff = unitNormal.dotProduct(pointLight) / Math.sqrt(x.getEntry(0) + x.getEntry(1) + x.getEntry(2));
        relS = s * coeff;
        relSVector = sVector.mapMultiply(coeff);
    }

    public void invertY() {
        relM.setRowVector(1, relM.getRowVector(1).mapMultiply(-1d));
    }

    public String getSVGPath() {
        StringBuilder sb = new StringBuilder();
        double[] v1 = relM.getColumn(0);
        double[] v2 = relM.getColumn(1);
        double[] v3 = relM.getColumn(2);

        /*
        Since Y is inverted on the screen, multiply it by -1
         */
        sb.append("M ")
          .append(v1[0])
          .append(" ")
          .append(v1[1])
          .append(" ")
          .append("L ")
          .append(v2[0])
          .append(" ")
          .append(v2[1])
          .append(" ")
          .append("L ")
          .append(v3[0])
          .append(" ")
          .append(v3[1])
          .append(" Z ");

        return sb.toString();
    }

    public double[] getXPoints() {
        return relM.getRow(0);
    }

    public double[] getYPoints() {
        return relM.getRow(1);
    }

    public double[] getPoints() {
        return new double[]{
                relM.getEntry(0, 0),
                relM.getEntry(1, 0),
                relM.getEntry(0, 1),
                relM.getEntry(1, 1),
                relM.getEntry(0, 2),
                relM.getEntry(1, 2)
        };
    }

    @Override
    public int compareTo(Triangle o) {
        return Double.compare(this.centroid.getEntry(2), o.centroid.getEntry(2));
    }

    public double getS() {
        return relS;
    }

    public int getSInt() {
        return (int) Math.abs(relS);
    }

    public RealVector getCentroid() {
        return centroid;
    }

    public void addSVector(RealVector sVector) {
        this.sVector = sVector;
        double d[] = sVector.toArray();
        this.s = (d[0] + d[1] + d[2]) / 3;
    }

    public double[] getRelSVector() {
        return relSVector.toArray();
    }
}
