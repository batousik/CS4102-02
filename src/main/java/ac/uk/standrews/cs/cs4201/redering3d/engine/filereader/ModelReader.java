package ac.uk.standrews.cs.cs4201.redering3d.engine.filereader;

import ac.uk.standrews.cs.cs4201.redering3d.engine.Triangle;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;

public class ModelReader {

    public static ArrayList<Triangle> readFaceShape() {
        String filepath = "3d/face/shape.txt";
        ArrayList<Triangle> triangles = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(
                    filepath)));

            String line;
            int counter = 0;
            double[][] triangle = new double[4][3];
            while (true) {
                line = reader.readLine();

                if (counter == 3) {

                    triangle[3][0] = 1d;
                    triangle[3][1] = 1d;
                    triangle[3][2] = 1d;

                    triangles.add(new Triangle(new Array2DRowRealMatrix(triangle)));
                    triangle = new double[4][3];
                    counter = 0;
                }
                if (line == null) break;
                if (line.equals("")) continue;

                String[] data = line.split(" ");
                triangle[0][counter] = Double.parseDouble(data[0]);
                triangle[1][counter] = Double.parseDouble(data[1]);
                triangle[2][counter] = Double.parseDouble(data[2]);
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return triangles;
    }

    public static ArrayList<RealVector> readFaceTexture() {
        String filepath = "3d/face/texture.txt";
        Path p = null;
        ArrayList<RealVector> data = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(
                    filepath)));
            String line;
            while (true) {
                line = reader.readLine();
                if (line == null) break;
                if (line.equals("")) continue;
                String[] values = line.split(" ");
                data.add(new ArrayRealVector(new double[]{
                        Double.valueOf(values[0]),
                        Double.valueOf(values[1]),
                        Double.valueOf(values[2])
                }));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }


    public static String readShape(String filepath) {
        return "";
    }

    public static String readTexture(String filepath) {
        return "";
    }

}
