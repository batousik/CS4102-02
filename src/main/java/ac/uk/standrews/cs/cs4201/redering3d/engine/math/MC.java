package ac.uk.standrews.cs.cs4201.redering3d.engine.math;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import static java.lang.Math.*;


public class MC {

    public static double radians(double degrees) {
        return degrees * PI / 180d;
    }

    public static double degrees(double radians) {
        return 180 * radians / Math.PI;
    }

    public static RealMatrix translation_matrix(double x, double y, double z) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        1d,
                        0d,
                        0d,
                        x
                },
                {
                        0d,
                        1d,
                        0d,
                        y
                },
                {
                        0d,
                        0d,
                        1d,
                        z
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix scale_matrix(double x, double y, double z) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        x,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        y,
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        z,
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix x_rot_matrix(double radians) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        1d,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        cos(radians),
                        -sin(radians),
                        0d
                },
                {
                        0d,
                        sin(radians),
                        cos(radians),
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix y_rot_matrix(double radians) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        cos(radians),
                        0d,
                        sin(radians),
                        0d
                },
                {
                        0d,
                        1d,
                        0d,
                        0d
                },
                {
                        -sin(radians),
                        0d,
                        cos(radians),
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix z_rot_matrix(double radians) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        cos(radians),
                        -sin(radians),
                        0d,
                        0d
                },
                {
                        sin(radians),
                        cos(radians),
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        1d,
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }


    public static RealMatrix rotation_around_unit_vector(double r, double x, double y, double z) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        cos(r) + x * x * (1 - cos(r)),
                        x * y * (1 - cos(r)) - z * sin(r),
                        x * z * (1 - cos(r)) + y * (sin(r)),
                        0
                },
                {
                        y * x * (1 - cos(r)) + z * sin(r),
                        cos(r) + y * y * (1 - cos(r)),
                        y * z * (1 - cos(r)) - x * sin(r),
                        0
                },
                {
                        z * x * (1 - cos(r)) - y * sin(r),
                        z * y * (1 - cos(r)) + x * sin(r),
                        cos(r) + z * z * (1 - cos(r)),
                        0
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix rotation_around_unit_vector_user_friendly(double r, double x, double y, double z) {
        RealMatrix K = null;

        return MatrixUtils.createRealIdentityMatrix(3).add(K.scalarMultiply(sin(r))).add(K.multiply(K)
                                                                                          .scalarMultiply(1d - cos(r)));

    }

    public static RealMatrix x_reflect_matrix() {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        -1d,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        1d,
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        1d,
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix y_reflect_matrix() {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        1d,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        -1d,
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        1d,
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix z_reflect_matrix() {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        1d,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        1d,
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        -1d,
                        0d
                },
                {
                        0d,
                        0d,
                        0d,
                        1d
                }
        });
    }

    public static RealMatrix orthographic_projection(double width, double height, double zFar, double zNear) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        1d / width,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        1d / height,
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        -2d / (zFar - zNear),
                        0d
                },
                {
                        0d,
                        0d,
                        -(zFar + zNear) / (zFar - zNear),
                        1d
                }
        });
    }

    public static RealMatrix perspective_projection(double width, double height, double zFar, double zNear) {
        return new Array2DRowRealMatrix(new double[][]{
                {
                        1d / width,
                        0d,
                        0d,
                        0d
                },
                {
                        0d,
                        1d / height,
                        0d,
                        0d
                },
                {
                        0d,
                        0d,
                        -2d / (zFar - zNear),
                        0d
                },
                {
                        0d,
                        0d,
                        -(zFar + zNear) / (zFar - zNear),
                        1d
                }
        });
    }
}
