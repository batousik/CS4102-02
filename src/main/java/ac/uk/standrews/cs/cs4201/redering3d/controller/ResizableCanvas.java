package ac.uk.standrews.cs.cs4201.redering3d.controller;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;


public class ResizableCanvas extends Canvas {
    public ResizableCanvas(Pane pane) {
        widthProperty().bind(pane.widthProperty());
        heightProperty().bind(pane.heightProperty());
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }
}
