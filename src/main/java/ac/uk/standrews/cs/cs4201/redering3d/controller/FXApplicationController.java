package ac.uk.standrews.cs.cs4201.redering3d.controller;


import ac.uk.standrews.cs.cs4201.redering3d.engine.Rendering3DEngine;
import ac.uk.standrews.cs.cs4201.redering3d.engine.Triangle;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class FXApplicationController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(FXApplicationController.class.getName());

    @FXML
    VBox renderingProgress;

    @FXML
    private Pane graphicHolder;

    @FXML
    private ChoiceBox<String> chooseProjectionCB;

    @FXML
    private Button resetToDefaultsButton;

    @FXML
    private Button redrawButton;

    @FXML
    private Slider rotationXSlider;

    @FXML
    private Slider rotationYSlider;

    @FXML
    private Slider rotationZSlider;

    @FXML
    private Slider zoomSlider;

    @FXML
    private Slider scalerSlider;

    @FXML
    private Label scalerSliderLabel;

    @FXML
    private Label rotationXSliderLabel;

    @FXML
    private Label rotationYSliderLabel;

    @FXML
    private Label rotationZSliderLabel;

    @FXML
    private Label zoomSliderLabel;

    private Rendering3DEngine r3e;

    private ResizableCanvas resizableCanvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        r3e = new Rendering3DEngine();
        renderingProgress.setVisible(false);
        graphicHolder.widthProperty().addListener((o, old, val) -> updateEngineDimensions(new Point2DDouble
                                                                                                  (graphicHolder
                                                                                                           .getWidth(),
                                                                                                            graphicHolder
                                                                                                                    .getHeight()
        )));
        graphicHolder.heightProperty().addListener((o, old, val) -> updateEngineDimensions(new Point2DDouble
                                                                                                   (graphicHolder
                                                                                                            .getWidth(),
                                                                                                             graphicHolder
                                                                                                                     .getHeight()
        )));
        chooseProjectionCB.getItems().addAll(Rendering3DEngine.PROJECTION.ORTHOGRAPHIC,
                                             Rendering3DEngine.PROJECTION.PERSPECTIVE
        );
        chooseProjectionCB.valueProperty().addListener((o, old, val) -> r3e.setProjection(val));
        redrawButton.setOnMouseClicked(create3DModel());
        resetToDefaultsButton.setOnMouseClicked(resetToDefaults());

        rotationXSlider.valueProperty().addListener((o, old, val) -> {
            r3e.setRotationX(val.intValue());
            rotationXSliderLabel.setText("X Rotation: " + val.intValue());

        });

        rotationYSlider.valueProperty().addListener((o, old, val) -> {
            r3e.setRotationY(val.intValue());
            rotationYSliderLabel.setText("Y Rotation: " + val.intValue());

        });

        rotationZSlider.valueProperty().addListener((o, old, val) -> {
            r3e.setRotationZ(val.intValue());
            rotationZSliderLabel.setText("Z Rotation: " + val.intValue());

        });

        zoomSlider.valueProperty().addListener((o, old, val) -> {
            r3e.setZoom(val.intValue());
            zoomSliderLabel.setText("Zoom: " + val.intValue());

        });

        scalerSlider.valueProperty().addListener((o, old, val) -> {
            double d = 1d / val.doubleValue();
            r3e.setScale(d);
            scalerSliderLabel.setText("Scalar Scaler: " + new DecimalFormat("#.#####").format(d));
        });

        resizableCanvas = new ResizableCanvas(graphicHolder);
        resizableCanvas.setCursor(Cursor.HAND);
        graphicHolder.getChildren().add(resizableCanvas);
        enableCanvasUpdates(resizableCanvas);
        create3DModel();
        createSimulationLoop();
        updateEngineDimensions(new Point2DDouble(graphicHolder.getWidth(), graphicHolder.getHeight()));
        doResetToDefaults();
        r3e.start();
        createSimulationLoop();
    }


    private void createSimulationLoop() {
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                try {
                    if (r3e.isWasInvalidated()) {
                        r3e.setWasInvalidated(false);
                        GraphicsContext gc = resizableCanvas.getGraphicsContext2D();
                        double width = resizableCanvas.getWidth();
                        double height = resizableCanvas.getHeight();
                        gc.clearRect(0, 0, width, height);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        new AnimationTimer() {


            @Override
            public void handle(long now) {
                if (!r3e.getTriangleBufferQueue().isEmpty()) {
                    GraphicsContext gc = resizableCanvas.getGraphicsContext2D();
                    try {
                        for (Triangle t : r3e.getTriangleBufferQueue().remove()) {
                            int s = t.getSInt();
//                            double[] c1 = t.getRelSVector();
                            Color c = Color.rgb(s, s, s, 1);
//                            Color c = Color.rgb(Math.abs((int) c1[0]), Math.abs((int) c1[1]), Math.abs((int) c1[2])
// , 1);
                            gc.setFill(c);
                            gc.setStroke(c);
                            gc.fillPolygon(t.getXPoints(), t.getYPoints(), 3);
                            gc.strokePolygon(t.getXPoints(), t.getYPoints(), 3);
                        }
                    } catch (NullPointerException npe) {
                        LOGGER.warning(npe.getLocalizedMessage());
                    }
                }
            }
        }.start();

        new AnimationTimer() {
            @Override
            public void handle(long now) {
                try {
                    renderingProgress.setVisible(r3e.isInvalidated());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void updateEngineDimensions(Point2DDouble p) {
        r3e.updateDimension(p);
    }

    private EventHandler<? super MouseEvent> create3DModel() {
        return (EventHandler<MouseEvent>) event -> r3e.setInvalidated(true);
    }

    private EventHandler<? super MouseEvent> resetToDefaults() {
        return (EventHandler<MouseEvent>) event -> doResetToDefaults();
    }

    private void doResetToDefaults() {
        r3e.setDefaults();
        rotationXSlider.setValue(0);
        rotationYSlider.setValue(0);
        rotationZSlider.setValue(0);
        zoomSlider.setValue(0);
        scalerSlider.setValue(250);
        chooseProjectionCB.setValue(Rendering3DEngine.PROJECTION.ORTHOGRAPHIC);
    }

    private void enableCanvasUpdates(final ResizableCanvas canvas) {
        final Point2DDouble dragDelta = new Point2DDouble();

        canvas.setOnMousePressed((MouseEvent mouseEvent) -> {
            // record xx delta distance for the drag and drop operation.
            dragDelta.x = mouseEvent.getX();
            dragDelta.y = mouseEvent.getY();
            canvas.setCursor(Cursor.MOVE);
        });

        canvas.setOnMouseReleased(mouseEvent -> {
            canvas.setCursor(Cursor.HAND);
            r3e.updateDeltaXY(new Point2DDouble(mouseEvent.getX() - dragDelta.x, mouseEvent.getY() - dragDelta.y));
        });
    }

    public void terminateEngine() {
        r3e.setRunning(false);
    }
}
