package ac.uk.standrews.cs.cs4201.redering3d.ui;


import ac.uk.standrews.cs.cs4201.redering3d.controller.FXApplicationController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class FXApplication extends Application {
    private FXMLLoader fxmlLoader;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage stage) throws IOException {
        URL location = getClass().getClassLoader().getResource("fxml/fxapplication.fxml");
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = fxmlLoader.load(location.openStream());

        Scene scene = new Scene(root, 800, 600);
        String material_css = this.getClass().getClassLoader().getResource("css/material-fx-v0_3.css").toExternalForm();
        String material_switch_css =
                this.getClass().getClassLoader().getResource("css/materialfx-toggleswitch.css").toExternalForm();
        String rendering3d_css = this.getClass().getClassLoader().getResource("css/rendering3d.css").toExternalForm();

        scene.getStylesheets().clear();
        scene.getStylesheets().add(material_css);
        scene.getStylesheets().add(material_switch_css);
        scene.getStylesheets().add(rendering3d_css);

        stage.setTitle("3D Rendering JavaFX Application");
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.show();
    }

    @Override
    public void stop() {
        ((FXApplicationController) fxmlLoader.getController()).terminateEngine();
    }
}